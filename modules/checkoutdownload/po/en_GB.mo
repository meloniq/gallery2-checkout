��          <      \       p      q   \   �   W   �   �  =     <  ^   M  W   �                   Download Size (pixels) Optionally users may be permitted to download items in their cart before payment is cleared. The limit on the number of times this order may be downloaded has already been reached. Project-Id-Version: Gallery: Checkout Downloads 0.1.3
Report-Msgid-Bugs-To: gallery-translations@lists.sourceforge.net
POT-Creation-Date: 2008-08-14 08:34+0100
PO-Revision-Date: 2008-08-14 08:39-0000
Last-Translator: Alec Myers <alec@alecmyers.com>
Language-Team: ENGLISH <gallery-translations@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-Language: English
X-Poedit-Country: UNITED KINGDOM
 Download Options Optionally users may be permitted to download items in their basket before payment is cleared. The limit on the number of times this order may be downloaded has already been reached. 