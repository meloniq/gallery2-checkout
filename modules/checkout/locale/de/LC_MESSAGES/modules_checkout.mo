��    0      �  C         (     )     ;     I     P     Y     k     t     �  "   �  	   �  
   �  }   �  �   Q     �     �     �     �                     $     8     >     F     e     y     �     �     �     �     �  >   �  (        ;     [     d     l  -   r     �     �  	   �  }   �  <   4  	   q     {  1   �     �  F  �     +
     C
  	   Q
     [
     d
     {
     �
     �
  (   �
     �
     �
  �   �
  ~   ~     �          
          +     B     I     U     p     v     ~  U   �     �     �     �       %   $     J  U   Y  +   �      �     �            ;        V     \     h  �   {  P   �     P     ^  ,   }     �                           )                .                              %             /   	      !   ,   &   (                     "                     0      '                                 +   $      #                    
   -   *    Add Photo To Cart Business Name Cancel Checkout Checkout Settings Commerce Continue shopping Continue to checkout Currency symbol to display in cart Drop-down Empty cart Enter the values you would like displayed during checkout. Anything left empty will not be used in the products/prices/paper. For smaller sets of products, 'table' view will work better.  For sites with larger product sets, 'drop-down' view is likely to be better. Info Item Modify Order No thumbnail Order Complete Paper Postage Amount Postage and Packing Price Product Product selection display type Products and Prices Quantity Remove Save Settings Save changes Settings saved successfully Shopping Cart Info Shopping cart module supporting different products and payment Step 1 - Select the Product and Quantity Step 2 - Confirm your selection Subtotal Summary Table Thank you - your order request has been sent. Title Total View Cart You can enter different paper types for your prints here, to be displayed as options to the user during the checkout process. You have %d item in your cart You have %d items in your cart Your Cart Your cart is currently empty. Your order will be processed as soon as possible. [checkout] Purchase item Project-Id-Version: 
POT-Creation-Date: 
PO-Revision-Date: 2007-03-02 18:27+0100
Last-Translator: Jay Hennessy (Elucidos Services Ltd) <gallery2-development@elucidos.com>
Language-Team: German <gallery-devel@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=iso-8859-1
Content-Transfer-Encoding: 8bit
 Foto in Warenkorb legen Gesch�ftsname Abbrechen Checkout Checkout Einstellungen Handel Weitere Produkte ausw�hlen Weiter zur Bestellung W�hrungs-Symbol zur Anzeige im Warenkorb Drop-Down Men� Warenkorb leeren Geben Sie die Preise an, die im Warenkorb angezeigt werden sollen. Leere Felder in Produkte/Preise/Papier werden nicht angezeigt. Tabellen-Anzeige ist f�r wenige Produkte besser. F�r Seiten mit mehreren Produkten ist normalerweise das Dropdown-Men� besser. Info Element Bestellung �ndern Keine Vorschau Bestellung erfolgreich Papier Postversand Postversand und Verpackung Preis Produkt Art der Produkt-Anzeige Produkte und Preise - ACHTUNG! SPRACHE IN ENGLISCH  �NDERN WEGEN EINES KOMMA-BUGS !!! Anzahl L�schen Einstellungen sichern �nderungen sichern Einstellungen erfolgreich gespeichert Warenkorb-Info Checkout - Warenkorbmodul unterst�tzt verschiedene Produkte, Preise und Zahlungsarten Stufe 1 - W�hlen Sie Produkt und Anzahl aus Stufe 2 - Best�tigen der Auswahl Summe Zusammenfassung Tabelle Vielen Dank - Ihre Bestellung wurde erfolgreich verschickt. Titel Gesamtsumme Warenkorb anzeigen Sie k�nnen verschiedene Papiertypen f�r ihre Fotos hier angeben, um dem User bei der Bestellung Alternativen zur Auswahl zu bieten. Sie haben %d Element in ihrem Warenkorb Sie haben %d Elemente in ihrem Warenkorb Ihr Warenkorb Ihr Warenkorb ist derzeit leer Ihre Bestellung wird schnellstens bearbeitet [checkout] Produkt kaufen 