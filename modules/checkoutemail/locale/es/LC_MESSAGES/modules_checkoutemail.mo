��    2      �  C   <      H     I     Q  0   X     �     �     �     �     �     �     �     �     �  !     8   0     i  !   w  :   �  
   �  n   �  	   N     X     q     ~     �     �  ;   �     �     �     �                      	   #  (   -     V     d  $   �     �     �  -   �  &   �  
          _      1   �     �     �     �  �  �  
   \
     g
  7   p
     �
     �
      �
     �
     �
     �
                 %   -  E   S     �  %   �  P   �     !  x   (     �  !   �     �     �     �     �  =   �     7  
   V     a     p     w     �     �     �  0   �     �  #   �  ,        .     ?  $   G  !   l  
   �     �  r   �  !        6     E     J        /          #               .      -                 +      '                       (              !      $          	      &   )       "          %         *   ,   1                  2                        
                    0    Address Cancel Caption of the button to appear on checkout page Checkout Checkout by Email Checkout by Email Settings Comments Commerce Complete checkout by email Country Customer Customer Details Customer and Delivery information Description of payment method to appear on checkout page Email Address Email address to send orders from Enter the values you would like displayed during checkout. First Name For each email address you want to send order notifications to, please also select the information to include. Last Name Links to original images No thumbnail Order Complete Paper Photo Please enter your details below to complete the transaction Postage and Packing Postal Address Postal/Zip Code Price Product Qty Quantity Recipient Recipient's Name (if different to above) Save Settings Settings saved successfully Subject line to appear on the emails Submit Order Summary Thank you - your order request has been sent. The following order has been submitted Thumbnails Title To send a mail to the customer's provided address, use the template address 'customer@gallery'. Your order will be processed as soon as possible. Zip each of Project-Id-Version: Gallery: Checkout by Email 0.0.9
Report-Msgid-Bugs-To: jerauskin@gmail.com
POT-Creation-Date: 2006-09-27 19:24+0100
PO-Revision-Date: 2007-02-20 21:34+0100
Last-Translator: Jay Hennessy (Elucidos Services Ltd) <gallery2-development@elucidos.com>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);
 Dirección Cancelar Título del botón que aparecerá en la página de caja Caja Caja por email Parámetros de la caja por email Comentarios Comercio Completar la caja por email Pais Cliente Datos del cliente Información del cliente y la entrega Descripción del método de pago que aparecerá en la página de caja Dirección email Email desde el que se manda el pedido Introduce los valores que desees visualizar durante el proceso de caja por email Nombre Se puede enviar tambíen notificación del pedido a estas direcciones de email, seleccionando la información a incluir. Apellido Enlaces a las imagenes originales No hay miniatura Pedido realizado Papel Foto Por favor instroduce tus datos para completar la transacción Gastos de correo y empaquetado Dirección Código postal Precio Producto Can Cantidad Buzón Nombre del buzón (si es diferente al de arriba) Guardar los parámetros Parámetros guardados correctamente Asunto que aparecerá en los emails enviados Enviar el pedido Resumen Gracias - Tu pedido ha sido enviado. Se ha enviado el siguiente pedido Miniaturas Título Para mandar un email a la dirección proporcionada por el usuario, usa la dirección plantilla 'customer@gallery'. Será procesado lo antes posible. Código postal cada de 